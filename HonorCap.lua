﻿local HONOR_CAP = 50000     -- Enter your cap here, reload UI or relog for changes to take effect
local WARN_BEFORE = 3500    -- Distance to the cap when warnings start
local UPDATE_DELAY = 0.1
local UPDATE_INTERVAL = 1
local HONOR_FOR_MARKS = 398

local honorGained = false
local honorGainedTime = GetTime()
local lastUpdatedTime = GetTime()
local _, currentHonor = GetPVPThisWeekStats()

local HonorCap = CreateFrame("Frame")
HonorCap:RegisterEvent("CHAT_MSG_COMBAT_HONOR_GAIN")
HonorCap:RegisterEvent("PLAYER_ENTERING_WORLD")         -- Remove or comment this code if you don't want to see warnings after a loading screen

HonorCap:SetScript("OnEvent", function()
    if event == "CHAT_MSG_COMBAT_HONOR_GAIN" then
        -- Calling HonorFrame_Update(1) right now does not return updated today's honor. Need a small delay.
        honorGainedTime = GetTime()
        honorGained = true
    elseif event == "PLAYER_ENTERING_WORLD" then  -- Print the warning after reloading just as a reminder
        HonorCap.CheckHonor(true)
    end
end)

HonorCap:SetScript("OnUpdate", function()
    if honorGained and GetTime() - honorGainedTime >= UPDATE_DELAY then
        HonorFrame_Update(1)
        honorGained = false
        HonorCap.CheckHonor()
    end
    -- Just in case honor tab was not updated after UPDATE_DELAY due to server/internet lag or other reasons, we update it once per UPDATE_INTERVAL
    if GetTime() - lastUpdatedTime >= UPDATE_INTERVAL then
        HonorFrame_Update(1)
        lastUpdatedTime = GetTime()
        HonorCap.CheckHonor()
    end
end)

function HonorCap.CheckHonor(printAnyway)
    local _, newHonor = GetPVPThisWeekStats()
    if newHonor > currentHonor or printAnyway then
        currentHonor = newHonor
        -- Close to the cap
        if currentHonor < HONOR_CAP and HONOR_CAP - currentHonor <= WARN_BEFORE then
            local honorTillCap = HONOR_CAP - currentHonor
            DEFAULT_CHAT_FRAME:AddMessage(format("%d honor until the cap (%d * %d + %d)",
                honorTillCap, HONOR_FOR_MARKS, floor(honorTillCap / HONOR_FOR_MARKS), mod(honorTillCap, HONOR_FOR_MARKS)), 255, 255, 0)
        -- Capped   
        elseif currentHonor == HONOR_CAP then
            DEFAULT_CHAT_FRAME:AddMessage("You are honor capped", 255, 0, 0)
        -- Over the cap
        elseif currentHonor >  HONOR_CAP then
            DEFAULT_CHAT_FRAME:AddMessage((currentHonor - HONOR_CAP).." honor over the cap", 255, 0, 0)
        end
    end
end