There are no in-game settings yet and they won't appear in the nearest future most likely (I don't have time now to learn how to do GUI and such in WoW).

So to set your honor cap you have to directly edit the first line of HonorCap.lua:
```lua
local HONOR_CAP = 50000
```  
You can also change the distance to the cap when warnings start (default is 3500) by editing the second line:
```lua
local WARN_BEFORE = 3500
```  
Reload UI or relog for changes to take effect (if you were in the game while editing).

You will get the warnings each time you get honor and after the loading screen if you distance to the cap is <= WARN_BEFORE (3500 by default). To disable the warnings after the loading screen, comment or remove this line:
```lua
HonorCap:RegisterEvent("PLAYER_ENTERING_WORLD")
```
The warnings look like this: ![Example screenshot](HonorCap.png)  
The number in parentheses is the same, but considering turning in BG marks. For example, on the picture I see that I can leave the BG, turn in marks 3 times and will get almost the required amount of honor (will be 26 honor below the cap).